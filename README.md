# Advent of Code, 2021

[Advent of Code 2021](https://adventofcode.com/2021) challenges, completed in the Rust programming language.

## About Advent of Code

Advent of Code is a set of challenges designed by [Eric Wastl](http://was.tl/).

*from the Advent of Code website's [about section](https://adventofcode.com/2021/about)*:

> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in [any](https://github.com/search?q=advent+of+code) programming language you like.
> People use them as a [speed contest](https://adventofcode.com/leaderboard), [interview](https://y3l2n.com/2018/05/09/interview-prep-advent-of-code/) [prep](https://twitter.com/dznqbit/status/1037607793144938497), [company training](https://twitter.com/pgoultiaev/status/950805811583963137), [university](https://gitlab.com/imhoffman/fa19b4-mat3006/wikis/home) [coursework](https://gribblelab.org/teaching/scicomp2021/index.html), [practice](https://twitter.com/mrdanielklein/status/936267621468483584) [problems](https://comp215.blogs.rice.edu/), or to [challenge each other](https://www.reddit.com/r/adventofcode/search?q=flair%3Aupping&restrict_sr=on).
>
> You don't need a computer science background to participate - just a little programming knowledge and some [problem solving skills](https://www.reddit.com/r/adventofcode/comments/7kd8jt/what_would_you_say_are_the_minimal_skills_for/dre0uu3/) will get you pretty far.
> Nor do you need a fancy computer; every problem has a solution that completes in at most 15 seconds on ten-year-old hardware.
>
> If you'd like to support Advent of Code, you can do so indirectly by helping to share it with others, or directly via [PayPal or Coinbase](https://adventofcode.com/2021/support).
>
>Advent of Code is a registered trademark in the United States.
